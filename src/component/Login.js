import React, {useState} from 'react'
import '../style/Login.css'

import 'antd/dist/antd.css';
import { Button, Input, Icon, Form } from 'antd';

import { useHistory } from 'react-router-dom'
import Loginform from './Loginform'

function Login() {

    const [user, setUser] = useState('')
    const history = useHistory();

    const handleSubmit = e => {
        console.log('user', user)
        history.push('/product_list')
    }

    const etst = Form.create()(Login)
    const { getFieldDecorator } = etst

    return (
        <div className='login'>
            <div className="login_container">
                <h1>Login</h1>
                <div className="login_span">
                    <span>管理後台</span>
                </div>
                <Loginform />

            </div>
        </div>
    )
}

export default Login
