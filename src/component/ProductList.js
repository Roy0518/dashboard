import React, { useState, useEffect } from 'react'
import '../style/ProductList.css'
import Header from './Header'

import { Button, Table, Icon } from 'antd'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getProduct, viewProduct } from '../action/product'
import ProductSearch from './ProductSearch'
  
//   const data = [
//     {
//         brand: 'nike',
//         name: 'Air Jordan 1 Retro High OG',
//         id: '555088-070',
//         price: '6300',
//         color: 'red/black/white',
//         size: 'US4.0'
//     },
//     {
//         brand: 'nike',
//         name: 'Air Jordan 1 Retro High OG',
//         id: '555088-070',
//         price: '6300',
//         color: 'red/black/white',
//         size: 'US4.5'
//     },
//     {
//         brand: 'nike',
//         name: 'Air Jordan 1 Retro High OG',
//         id: '555088-070',
//         price: '6300',
//         color: 'red/black/white',
//         size: 'US5.0'
//     },
//     {
//         brand: 'nike',
//         name: 'DIOR x Air Jordan 1 Retro High OG',
//         id: 'CN8607-002',
//         price: '60000',
//         color: 'red/black/white',
//         size: 'US5.0'
//     },
//   ];

  function onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
  }

function ProductList() {

    const data = useSelector(state => state.product)
    const history = useHistory();
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)


    const handleEdit = (item) => {
        dispatch(viewProduct(item))
        console.log(item)
        history.push('/product_edit/')
    }
    const handleView = (item) => {
        dispatch(viewProduct(item))
        console.log(item)
        history.push('/product_info/')
    }

    const columns = [
        {
            title: '品牌名稱',
            dataIndex: 'brand',
            align: 'center',
            // sorter: (a, b) => a.name.length - b.name.length,
            //   defaultSortOrder: 'descend',
        },
        {
            title: '商品名稱',
            dataIndex: 'name',
            //   defaultSortOrder: 'descend',
            sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: '商品編號',
            dataIndex: 'id',
            align: 'center',
            sorter: (a, b) => a.id - b.id,
            // sortDirections: ['descend', 'ascend'],
        },
        {
            title: '價格',
            dataIndex: 'price',
            align: 'center',
            sorter: (a, b) => a.price - b.price,
            // sortDirections: ['descend', 'ascend'],
        },
        {
            title: '規格一 ( 顏色 )',
            dataIndex: 'color',
            align: 'center',
        },
        {
            title: '規格二 ( 大小 )',
            dataIndex: 'size',
            align: 'center',
        },
        {
            title: '動作',
            dataIndex: '',
            align: 'center',
            key: 'x',
            render(text, record, index) {

                return (
                    <div className='action'>
                        <Icon type="edit" onClick={()=> handleEdit(record)} />
                        <Icon type="eye" onClick={() => handleView(record)} />
                    </div>
                )
            },
        }, 
      ];



    useEffect(() => {
        setLoading(true)
        fetch('data.json')
            .then((response) => {
                return response.json()
            })
            .then(function(res){
                dispatch(getProduct(res.data))
                setLoading(false)   
            })
    }, [])

    return (
        <div className='productList'>
            <Header />
            <div className="productList_search">
                <ProductSearch />
            </div>
            
            {/* <div className="productList_search">
                <div className="search_contaner">
                    <div className="product_id">
                        <label htmlFor="">商品編號</label>
                        <input type="text"/>
                    </div>
                    <div className="product_name">
                        <label htmlFor="">商品名稱</label>
                        <input type="text"/>                    
                    </div>
                    <Button icon="search" className='search_btn'>搜尋</Button>
                </div>
            </div> */}

            <div className="List">
                <Table columns={columns} rowKey={(record, index) => index} dataSource={data} onChange={onChange} loading={loading} pagination={{position: 'top',size: 'small' , showSizeChanger: true, showQuickJumper: true}} />
            </div>

        </div>
    )
}

export default ProductList
