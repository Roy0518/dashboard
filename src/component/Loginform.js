import React from "react";
import { Form, Input, Button, Icon } from "antd";
import { withRouter } from 'react-router-dom';

const FormItem = Form.Item;

class FormLogin extends React.Component{

    handleSubmit = e => {
        this.props.history.push("/product_list");
    }

    render(){
        const { getFieldDecorator } = this.props.form;

        return (
            <div>
                <Form style={{width:300}} className='formLogin'>
                    <FormItem>
                        {
                            getFieldDecorator('userName',{
                                initialValue:'',
                                rules:[
                                    {
                                        required:true,
                                        message:'請輸入帳號'
                                    },
                                    {
                                        min:5,
                                        message:'長度過短，'
                                    },
                                    {
                                        pattern:new RegExp('[A-Za-z]{1}'),
                                        message:'首字需英文'
                                    }
                                ]
                            })(
                                <Input
                                    className="user"
                                    placeholder="使用者帳號 3-20碼"
                                    required
                                    minLength={3}
                                    maxLength={20}
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                />
                            )
                        }
                    </FormItem>
                    <FormItem>
                        {
                            getFieldDecorator('userPwd', {
                                initialValue: '',
                                rules: [
                                    {
                                        required:true,
                                        message:'請輸入密碼'
                                    },
                                ]
                            })(
                                <Input.Password
                                    className='password'
                                    required
                                    placeholder="密碼 6-20碼，英文字母需區分大小寫" 
                                />
                            )
                        }
                    </FormItem>
                    <FormItem>
                        {
                            getFieldDecorator('login', {
                                valuePropName:'checked',
                                initialValue: true
                            })(
                                <Button className='login_btn' block onClick={this.handleSubmit} >登入</Button>
                            )
                        }
                    </FormItem>
                    <FormItem>
                        <p  className='forget' href="">忘記密碼</p>
                    </FormItem>
                </Form>
            </div>
        );
    }
}
export default withRouter(Form.create()(FormLogin));