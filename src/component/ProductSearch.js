import React from "react";
import { Form, Input, Button } from "antd";

const FormItem = Form.Item;

class ProductSearch extends React.Component{

    handleSubmit = e => {
        this.props.history.push("/product_list");
    }

    render(){
        const { getFieldDecorator } = this.props.form;

        return (
            <div className='productSearch'>
                <Form className='formLogin'>
                    <FormItem label="品牌名稱 ">
                        {
                            getFieldDecorator('userName',{
                                initialValue:'',
                                rules:[
                                    {
                                        pattern:new RegExp('^([a-zA-Z]+\d+|\d+[a-zA-Z]+)[a-zA-Z0-9]*$'),
                                        message:'請輸入正確值'
                                    }
                                ]
                            })(
                                <Input
                                    className="user"
                                    placeholder="請輸入"
                                />
                            )
                        }
                    </FormItem>
                    <FormItem label="商品名稱 ">
                        {
                            getFieldDecorator('userPwd', {
                                initialValue: '',
                                rules: []
                            })(
                                <Input
                                    placeholder="請輸入" 
                                />
                            )
                        }
                    </FormItem>
                    <FormItem>
                        {
                            getFieldDecorator('login', {
                                valuePropName:'checked',
                                initialValue: true
                            })(
                                <Button className='login_btn' >搜尋</Button>
                            )
                        }
                    </FormItem>
                </Form>
            </div>
        );
    }
}
export default Form.create()(ProductSearch);