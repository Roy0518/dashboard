import React, {useState, useEffect } from 'react'
import '../style/ProductEdit.css'

import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { editProduct } from '../action/product'

import {
    Form,
    Input,
    Button,
  } from 'antd';
import Header from './Header';

function ProductEdit() {

    const viewData = useSelector(state => state.viewProduct)
    const history = useHistory();
    const dispatch = useDispatch()

    const [product, setProduct] = useState({
        brand: viewData.brand,
        name: viewData.name,
        id: viewData.id,
        price: viewData.price,
        color: viewData.color,
        size: viewData.size,
    })

    const formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 10 },
    }

    return (
        <div className='productEdit'>
            <Header />

            <div className="productEdit_container">
                <div className="productEdit_header">
                    <p>商品資訊</p>
                </div>

                <div className="productEdit_content">
                    <Form>
                        <Form.Item label="品牌名稱 " {...formItemLayout}>
                            <Input placeholder="品牌名稱" defaultValue={viewData.brand} onChange={(e) => { setProduct({...product, brand: e.target.value}); console.log('product',product) }} />
                        </Form.Item>
                        <Form.Item label="商品名稱 " {...formItemLayout}>
                            <Input placeholder="商品名稱" defaultValue={viewData.name} onChange={(e) => { setProduct({...product, name: e.target.value}); console.log('product',product) }} />
                        </Form.Item>
                        <Form.Item label="商品編號 " {...formItemLayout}>
                            <Input placeholder="商品編號" defaultValue={viewData.id} onChange={(e) => { setProduct({...product, id: e.target.value}); console.log('product',product) }} />
                        </Form.Item>
                        <Form.Item label="價格 " {...formItemLayout}>
                            <Input placeholder="價格" defaultValue={viewData.price} onChange={(e) => { setProduct({...product, price: e.target.value}); console.log('product',product) }} />
                        </Form.Item>
                        <Form.Item label="規格一 " {...formItemLayout}>
                            <Input placeholder="規格一" defaultValue={viewData.color} onChange={(e) => { setProduct({...product, color: e.target.value}); console.log('product',product) }} />
                        </Form.Item>
                        <Form.Item label="規格二 " {...formItemLayout}>
                            <Input placeholder="規格二" defaultValue={viewData.size} onChange={(e) => { setProduct({...product, size: e.target.value}); console.log('product',product) }} />
                        </Form.Item>
                        <Form.Item className='submit_btn'>
                            <Button type="primary" onClick={() => {history.push('/product_list')}}>儲存</Button>
                        </Form.Item>
                    </Form>
                </div>

            </div>
        </div>
    )
}

export default ProductEdit

