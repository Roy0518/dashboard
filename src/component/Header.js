import React from 'react'
import '../style/Header.css'

import { Button, Input, Icon, Menu, Avatar } from 'antd';

function Header() {

    const { SubMenu } = Menu;

    return (
        <div className='header'>
            <div className="header_user">
                <Icon type="menu" />
            </div>
        </div>
    )
}

export default Header
