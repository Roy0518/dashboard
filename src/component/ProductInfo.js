import React from 'react'
import '../style/ProductInfo.css'
import Header from './Header'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import {
    Form,
    Input,
    Button,
  } from 'antd';

function ProductInfo() {

    const viewData = useSelector(state => state.viewProduct)
    const history = useHistory();
    const dispatch = useDispatch()

    const formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 10 },
    }

    return (
        <div className='productInfo'>
            <Header />
            <div className="productInfo_container">
                <div className="productInfo_header">
                    <p>基本資料</p>
                </div>

                <div className="productInfo_content">
                    <Form>
                        <Form.Item label="品牌名稱 " {...formItemLayout}>
                            <p>{viewData.brand}</p>
                        </Form.Item>
                        <Form.Item label="商品名稱 " {...formItemLayout}>
                            <p>{viewData.name}</p>
                        </Form.Item>
                        <Form.Item label="商品編號 " {...formItemLayout}>
                            <p>{viewData.id}</p>
                        </Form.Item>
                        <Form.Item label="價格 " {...formItemLayout}>
                            <p>{viewData.price}</p>
                        </Form.Item>
                        <Form.Item label="規格一 " {...formItemLayout}>
                            <p>{viewData.color}</p>
                        </Form.Item>
                        <Form.Item label="規格二 " {...formItemLayout}>
                            <p>{viewData.size}</p>
                        </Form.Item>
                        <Form.Item className='submit_btn'>
                            <Button type="primary" onClick={()=> {history.push('/product_list')}}>返回</Button>
                        </Form.Item>
                    </Form>
                </div>

            </div>
        </div>
    )
}

export default ProductInfo
