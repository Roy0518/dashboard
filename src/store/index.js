import { createStore } from "redux";
import productReducer from '../reducer/product'

const store = createStore(productReducer)

export default store