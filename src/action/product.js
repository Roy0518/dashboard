export const GET_PRODUCT = 'GET_PRODUCT';
export const VIEW_PRODUCT = 'VIEW_PRODUCT'
export const EDIT_PRODUCT = 'EDIT_PRODUCT'

export const getProduct = (product) => ({
    type: GET_PRODUCT,
    payload: {
        product
    }
})

export const viewProduct = (product) => ({
    type: VIEW_PRODUCT,
    payload: {
        product
    }
})

export const editProduct = (product) => ({
    type: EDIT_PRODUCT,
    payload: {
        product
    }
})