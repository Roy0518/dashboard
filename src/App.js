import './App.css';
import 'antd/dist/antd.css';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Login from './component/Login'
import ProductList from './component/ProductList'
import ProductInfo from './component/ProductInfo'
import ProductEdit from './component/ProductEdit'

function App() {
  return (
    <div className="App">
      {/* <Login /> */}
      {/* <ProductList /> */}
      {/* <ProductInfo /> */}
      {/* <ProductEdit /> */}
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/product_list" component={ProductList}></Route>
          <Route path="/product_info/" component={ProductInfo}></Route>
          <Route path="/product_edit" component={ProductEdit}></Route>
        </Switch>
      </Router>

    </div>
  );
}

export default App;
