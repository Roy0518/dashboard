  import * as actions from '../action/product';


const initialState = {
    product: [],
    viewProduct: [
        {
            brand: 'nike',
            name: 'Air Jordan 1 Retro High OG',
            id: '555088-070',
            price: '6300',
            color: 'red/black/white',
            size: 'US4.5'
        },
    ],
}

const productReducer = (state = initialState, action) => {
    console.log(action.type)
    switch(action.type) {
        case actions.GET_PRODUCT:
            return {
                ...state,
                product: 
                    action.payload.product,
            }
        case actions.VIEW_PRODUCT:
            return {
                ...state,
                viewProduct: action.payload.product
                
            }
        case actions.EDIT_PRODUCT:

        default:
            return state
    }
}

export default productReducer